import * as AT from '../action-types'
const taskAction = {
    queryTaskList() {
        return {
            type: AT.TASK_QUERY_LIST
        }
    }
}
export default taskAction