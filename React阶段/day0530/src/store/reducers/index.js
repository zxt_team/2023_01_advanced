import { combineReducers } from 'redux'
import voteReducer from './voteReducer'
import taskReducer from './taskReducer'

const reducer = combineReducers({
    vote: voteReducer,
    task: taskReducer
})
export default reducer
