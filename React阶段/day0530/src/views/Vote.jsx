import React from "react"
import VoteStyle from "./VoteStyle"
import VoteMain from "./VoteMain"
import VoteFooter from "./VoteFooter"
import { connect } from '@/myreactredux'

const Vote = function Vote(props) {
    let { title, supNum, oppNum } = props
    return <VoteStyle>
        <h2 className="title">
            {title}
            <span>{supNum + oppNum}</span>
        </h2>
        <VoteMain />
        <VoteFooter />
    </VoteStyle>
}
export default connect(
    state => state.vote
)(Vote)

/* 
 connect：react-redux 提供的高阶函数
   + 获取基于 Provider 放在上下文中的 store 对象
   + 把让组件更新的办法自动加入到事件池中
   export default connect(
    [mapStateToProps],
    [mapDispatchToProps]
   )(需要渲染的组件)

 [mapStateToProps]：内部获取总的公共状态，把需要的公共状态，最后基于 属性 传递给需要渲染的组件
   + 类型：函数 或者 null/undefined
   connect(
     state=>{
        // state：总的状态
        return {
            // 返回对象中的信息，最后会作为属性传递给组件
            ...
        }
     }
   )
*/