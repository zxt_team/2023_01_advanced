import React from 'react'
import ReactDOM from 'react-dom'
import Demo from './views/DemoEvent'
import './index.less';

ReactDOM.render(
	<Demo />,
	document.getElementById('root')
)