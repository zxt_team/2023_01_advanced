import React from "react"

export default class Demo extends React.Component {
    state = {
        x: 10,
        y: 20,
        z: 30
    }

    handler = () => {
        /* this.setState({ x: 100 })
        console.log(this.state.x) //10
        this.setState({ y: 200 })
        console.log(this.state.y) //20
        this.setState({ z: 300 })
        console.log(this.state.z) //30 */

        setTimeout(() => {
            this.setState({ x: 100 })
            console.log(this.state.x) //100
            this.setState({ y: 200 })
            console.log(this.state.y) //200
            this.setState({ z: 300 })
            console.log(this.state.z) //300
        }, 1000)
    }

    render() {
        console.log('render')
        let { x, y, z } = this.state
        return <div className="box">
            <p> {x} - {y} - {z} </p>
            <button type="primary" size="small" onClick={this.handler}>按钮</button>
        </div>
    }
}