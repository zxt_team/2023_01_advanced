import React from 'react'
import ReactDOM from 'react-dom/client'
/* ANTD */
import { ConfigProvider } from 'antd'
import zhCN from 'antd/locale/zh_CN'
/* REDUX */
import { Provider } from 'react-redux'
import store from './store'
/* 组件&样式 */
import './index.less'
import Demo from './views/Demo'
import Vote from './views/Vote'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <ConfigProvider locale={zhCN}>
    <Provider store={store}>
      <Demo />
    </Provider>
  </ConfigProvider>
)