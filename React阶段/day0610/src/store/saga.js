import { takeEvery, call, put } from 'redux-saga/effects'
import * as AT from './action-types'
import API from '@/api'

// 工作函数「必须是一个生成器函数」
const incrementWorking = function* incrementWorking(action) {
    // action：组件中派发传递的派发对象
    // 发送异步请求
    yield call(API.delay, 1000)  //等价于 await API.delay(1000)
    // 异步请求结束，需要向reducer派发任务
    yield put({
        type: AT.DEMO_INCREMENT,
        step: action.step
    })
}

// 监听器函数「必须是一个生成器函数」
export default function* saga() {
    yield takeEvery(AT.DEMO_INCREMENT + '@SAGA', incrementWorking)
}