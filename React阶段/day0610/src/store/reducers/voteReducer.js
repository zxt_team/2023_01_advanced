import * as AT from '../action-types'
let initial = {
    supNum: 10,
    oppNum: 5
}
export default function voteReducer(state = initial, action) {
    state = { ...state }
    let { type, step = 1 } = action
    switch (type) {
        case AT.VOTE_SUPPORT:
            state.supNum += step
            break
        case AT.VOTE_OPPOSE:
            state.oppNum += step
            break
        default:
    }
    return state
}