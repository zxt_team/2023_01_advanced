import * as AT from '../action-types'
let initial = {
    num: 0
}
export default function demoReducer(state = initial, action) {
    state = { ...state }
    let { type, step = 1 } = action
    switch (type) {
        case AT.DEMO_INCREMENT:
            state.num += step
            break
        default:
    }
    return state
}