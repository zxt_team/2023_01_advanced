import React from "react"
import styled from "styled-components"
import { Button } from 'antd'
import { useSelector, useDispatch } from 'react-redux'
import * as AT from '@/store/action-types'

/* 样式处理 */
const DemoStyle = styled.div`
    margin: 40px auto;
    padding: 20px;
    width: 200px;
    border: 1px solid #DDD;
    .num{
        display: block;
        font-size: 20px;
        line-height: 40px;
    }
    .ant-btn{
        margin-right: 10px;
    }
`

const Demo = function Demo() {
    let { num } = useSelector(state => state.demo)
    const disptach = useDispatch()

    // 同步派发
    const handleSync = () => {
        disptach({
            type: AT.DEMO_INCREMENT
        })
    }

    // 异步派发
    const handleAsync = () => {
        disptach({
            type: AT.DEMO_INCREMENT + '@SAGA',
            step: 10
        })
    }

    return <DemoStyle>
        <span className="num">{num}</span>
        <Button type="primary" onClick={handleSync}>同步按钮</Button>
        <Button type="primary" danger onClick={handleAsync}>异步按钮</Button>
    </DemoStyle>
}
export default Demo