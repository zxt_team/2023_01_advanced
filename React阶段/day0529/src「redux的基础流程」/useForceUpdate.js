import { useState, useEffect } from 'react'

export default function useForceUpdate(store) {
    let [, setRandom] = useState(+new Date())
    let unsubscribe
    useEffect(() => {
        // 组件第一次渲染完毕，把让组件更新的办法放在事件池中
        // 执行subscribe会返回unsubscribe，目的是移除刚才加入事件池中的方法
        unsubscribe = store.subscribe(() => {
            setRandom(+new Date())
        })
        return () => {
            // 组件销毁的时候，把放在事件池中的方法移除掉
            unsubscribe()
            unsubscribe = null
        }
    }, [])
    // 手动返回一个remove函数：等待后期执行remove的时候，可以把事件池中的方法移除「必须在第一次渲染完毕后再执行remove函数，因为在那之后，unsubscribe才会有值」
    return function remove() {
        if (unsubscribe) unsubscribe()
    }
}
