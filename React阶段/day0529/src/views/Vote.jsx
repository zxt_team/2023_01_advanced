import React, { useContext } from "react"
import VoteStyle from "./VoteStyle"
import VoteMain from "./VoteMain"
import VoteFooter from "./VoteFooter"
import Theme from "@/Theme"
import useForceUpdate from "@/useForceUpdate"

const Vote = function Vote() {
    const { store } = useContext(Theme)

    // 获取公共状态信息
    let { supNum, oppNum, title } = store.getState().vote
    useForceUpdate(store)

    return <VoteStyle>
        <h2 className="title">
            {title}
            <span>{supNum + oppNum}</span>
        </h2>
        <VoteMain />
        <VoteFooter />
    </VoteStyle>
}

export default Vote