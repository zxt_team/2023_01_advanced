/* 投票板块的reducer */

let initial = {
    title: 'React不是很难的!',
    supNum: 10,
    oppNum: 5
}
export default function voteReducer(state = initial, action) {
    let { type } = action
    switch (type) {
        case 'sup':
            state.supNum++
            break
        case 'opp':
            state.oppNum++
            break
        default:
    }
    return state
}