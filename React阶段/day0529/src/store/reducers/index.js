/* 把各个板块下的REDUCER合并在一起 */
import { combineReducers } from 'redux'
import voteReducer from './voteReducer'
import taskReducer from './taskReducer'

const reducer = combineReducers({
    vote: voteReducer,
    task: taskReducer
})
export default reducer

/* 
combineReducers底层处理的机制
  @1 首先store容器中的公共状态，会按照设定的各个模块名，分别管理各模块下的状态
     combineReducers({
        模块名:对应的reducer
     })
     state = {
        vote: {
          title,
          supNum,
          oppNum
        },
        task: {
          title,
          list
        }
     }
     这样我们再基于 store.getState() 获取的就是总状态了，想获取具体的信息，还需要找到各个模块，再去访问处理！

  @2 每一次 dispatch 派发的时候，会把所有模块的reducer都执行一遍
    例如：store.dispatch({ type:'sup' })
      先通知 voteReducer 执行 -> 
        state.vote = voteReducer(state.vote, action)
      再通知 taskReducer 执行 ->
        state.task = taskReducer(state.task, action)
      ...
*/