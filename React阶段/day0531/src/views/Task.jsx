import { useState, useEffect, useMemo } from 'react'
import styled from 'styled-components'
import { Button, Tag, Table, Popconfirm, Modal, Form, Input, DatePicker, message } from 'antd'
import _ from '@/assets/utils'
import dayjs from 'dayjs'
import API from '@/api'
import { connect } from 'react-redux'
import action from '@/store/actions'

/* 组件样式 */
const TaskStyle = styled.div`
    box-sizing: border-box;
    margin: 0 auto;
    width: 800px;

    .header-box{
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 10px 0;
        border-bottom: 1px solid #DDD;

        .title{
            font-size: 20px;
            font-weight: normal;
        }
    }

    .tag-box{
        margin: 15px 0;

        .ant-tag{
            padding: 5px 15px;
            margin-right: 15px;
            font-size: 14px;
            cursor: pointer;
        }
    }

    .ant-btn-link{
        padding: 4px 5px;
    }

    .ant-modal-header{
        margin-bottom: 20px;
    }
`

/* 组件视图 */
const Task = function Task(props) {
    // 获取属性信息
    let { list, queryAllList, removeTask, updateTask } = props

    // 定义表格列
    const columns = [{
        title: '编号',
        dataIndex: 'id',
        width: '6%',
        align: 'center'
    }, {
        title: '任务描述',
        dataIndex: 'task',
        width: '50%',
        ellipsis: true
    }, {
        title: '状态',
        dataIndex: 'state',
        width: '10%',
        align: 'center',
        render: text => +text === 1 ? '未完成' : '已完成'
    }, {
        title: '完成时间',
        width: '16%',
        align: 'center',
        render(text, record) {
            let { state, time, complete } = record
            time = +state === 1 ? time : complete
            return _.formatTime(time, '{1}/{2} {3}:{4}')
        }
    }, {
        title: '操作',
        width: '18%',
        render(text, record) {
            let { state, id } = record
            return <>
                <Popconfirm title="您确定要删除此任务吗?"
                    onConfirm={handleRemove.bind(null, id)}>
                    <Button type="link" danger>删除</Button>
                </Popconfirm>

                {+state === 1 ?
                    <Popconfirm title="您确定要把此任务设置为已完成吗?"
                        onConfirm={handleUpdate.bind(null, id)}>
                        <Button type="link">完成</Button>
                    </Popconfirm> :
                    null
                }
            </>
        }
    }]

    // 定义状态
    let [loading, setLoading] = useState(false),
        [selectedIndex, setSelectedIndex] = useState(0)
    let [modalVisible, setModalVisible] = useState(false),
        [confirmLoading, setConfirmLoading] = useState(false)
    let [formIns] = Form.useForm()

    // 组件第一次渲染完毕，先判断一下redux中是否存在全部任务，如果没有存在，则完成一次异步派发
    useEffect(() => {
        (async () => {
            if (!list) {
                // 默认派发一次
                setLoading(true)
                await queryAllList() //只有使用的是redux-promise中间件，才保证派发返回的结果是promise实例，才可以用await做监听
                setLoading(false)
            }
        })()
    }, [])

    // 依赖于redux中的全部任务「list」 和 当前选中的状态「selectedIndex」，筛选出表格需要数据
    let data = useMemo(() => {
        let arr = list || []
        if (selectedIndex === 1 || selectedIndex === 2) {
            arr = arr.filter(item => {
                return +item.state === selectedIndex
            })
        }
        return arr
    }, [list, selectedIndex])

    // 关闭Modal对话框
    const closeModal = () => {
        setModalVisible(false)
        setConfirmLoading(false)
        formIns.resetFields()
    }

    // 确认新增任务
    const submit = async () => {
        try {
            await formIns.validateFields()
            let { task, time } = formIns.getFieldsValue()
            time = time.format('YYYY-MM-DD HH:mm:ss')
            setConfirmLoading(true)
            let { code } = await API.insertTaskInfo(task, time)
            if (+code === 0) {
                message.success('恭喜您，新增成功~')
                closeModal()
                // 新增成功后，需要派发异步任务，同步最新的数据到redux中
                setLoading(true)
                await queryAllList()
                setLoading(false)
            } else {
                message.error('很遗憾，新增失败，请稍后再试~')
            }
        } catch (_) { }
        setConfirmLoading(false)
    }

    // 删除任务
    const handleRemove = async (id) => {
        try {
            let { code } = await API.removeTaskById(id)
            if (+code === 0) {
                message.success('恭喜您，删除成功')
                // 派发删除的同步任务
                removeTask(id)
                return
            }
            message.error('很遗憾，删除失败，请稍后再试')
        } catch (_) { }
    }

    // 修改任务
    const handleUpdate = async (id) => {
        try {
            let { code } = await API.updateTaskById(id)
            if (+code === 0) {
                message.success('恭喜您，修改成功')
                // 派发修改的同步任务
                updateTask(id)
                return
            }
            message.error('很遗憾，修改失败，请稍后再试')
        } catch (_) { }
    }

    return <TaskStyle>
        <div className="header-box">
            <h2 className="title">TASK OA 任务管理系统</h2>
            <Button type="primary" onClick={() => setModalVisible(true)}>新增任务</Button>
        </div>

        <div className="tag-box">
            {['全部', '未完成', '已完成'].map((item, index) => {
                return <Tag key={index}
                    color={selectedIndex === index ? '#1677ff' : ''}
                    onClick={() => {
                        if (selectedIndex === index) return
                        setSelectedIndex(index)
                    }}>
                    {item}
                </Tag>
            })}
        </div>

        <Table columns={columns} dataSource={data} loading={loading} pagination={false} rowKey="id" size="middle" />

        <Modal title="新增任务窗口"
            okText="确认提交"
            keyboard={false}
            maskClosable={false}
            getContainer={false}
            confirmLoading={confirmLoading}
            open={modalVisible}
            afterClose={closeModal}
            onCancel={closeModal}
            onOk={submit}>

            <Form colon={false}
                layout="vertical"
                validateTrigger="onBlur"
                form={formIns}
                initialValues={{
                    task: '',
                    time: dayjs().add(1, 'day')
                }}>
                <Form.Item name="task" label="任务描述"
                    rules={[
                        { required: true, message: '任务描述是必填项' }
                    ]}>
                    <Input.TextArea rows={4} />
                </Form.Item>
                <Form.Item name="time" label="预期完成时间"
                    rules={[
                        { required: true, message: '请先选择预期完成时间' }
                    ]}>
                    <DatePicker showTime />
                </Form.Item>
            </Form>

        </Modal>
    </TaskStyle >
}
export default connect(
    state => state.task,
    action.task
)(Task)