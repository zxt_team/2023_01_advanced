import http from "./http"

// 获取指定状态下的任务列表
const queryTaskList = (state = 0) => {
    return http.get('/getTaskList', {
        params: {
            state
        }
    })
}

// 新增任务
const insertTaskInfo = (task, time) => {
    return http.post('/addTask', {
        task,
        time
    })
}

// 删除任务
const removeTaskById = (id) => {
    return http.get('/removeTask', {
        params: {
            id
        }
    })
}

// 完成任务
const updateTaskById = (id) => {
    return http.get('/completeTask', {
        params: {
            id
        }
    })
}

// 模拟接口
const delay = function delay(interval = 1000) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve()
        }, interval)
    })
}

/* 暴露API */
const API = {
    delay,
    queryTaskList,
    insertTaskInfo,
    removeTaskById,
    updateTaskById
}
export default API