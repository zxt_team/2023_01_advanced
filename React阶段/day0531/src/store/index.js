import { legacy_createStore, applyMiddleware } from 'redux'
import reduxLogger from 'redux-logger' //输出派发日志「方便调试」
import reduxThunk from 'redux-thunk'
import reduxPromise from 'redux-promise'
import reducer from './reducers'

// 使用中间件
const env = process.env.NODE_ENV
const middleware = []
if (env !== 'production') middleware.push(reduxLogger)
middleware.push(reduxPromise)
middleware.push(reduxThunk)

// 创建STORE
const store = legacy_createStore(
    reducer,
    applyMiddleware(...middleware)
)
export default store