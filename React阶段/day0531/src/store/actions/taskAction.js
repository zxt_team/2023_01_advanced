import * as AT from '../action-types'
import API from '@/api'

const taskAction = {
    /* 获取全部任务的异步派发 */
    async queryAllList() {
        let list = []
        try {
            let result = await API.queryTaskList()
            if (+result.code === 0) {
                list = result.list
            }
        } catch (_) { }
        // 返回派发的对象
        return {
            type: AT.TASK_QUERY_ALL_LIST,
            list
        }
    },
    /* 删除任务的同步派发 */
    removeTask(id) {
        return {
            type: AT.TASK_REMOVE,
            id
        }
    },
    /* 修改任务的同步派发 */
    updateTask(id) {
        return {
            type: AT.TASK_UPDATE,
            id
        }
    }
}
export default taskAction