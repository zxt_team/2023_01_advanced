import * as AT from '../action-types'
import API from '@/api'

const demoAction = {
    /* 
    基于reduxThunk中间件处理异步派发 
      @1 也对 disptach 进行了重写
        + 单击按钮的时候，是基于重写的 disptach 进行派发的
        + 执行 action.demo.increment 获取到的是一个函数
        + thunk中间件内部，会把获取的这个函数执行，并且把 store.dispatch 传递给这个函数
      @2 我们一般要在这个函数中发送异步请求，等待请求成功后，需要手动基于 store.dispatch 进行派发
    */
    increment(step = 1) {
        return async disptach => {
            // disptach:store.dispatch
            await API.delay()
            disptach({
                type: AT.DEMO_INCREMENT,
                step
            })
        }
    }

    /* 
    基于 reduxPromise 实现异步派发
      当我们应用了 reduxPromise 中间件，点击按钮的时候，是基于其重写的 dispatch 实现派发的
      @1 disptach(action.demo.increment(10))
        + disptach是重写的函数
        + action.demo.increment执行的结果是一个 promise 实例
        + 在 reduxPromise 中间件的内部会监测 promise 实例的结果
      @2 如果实例是失败态，则不做任何处理！如果实例是成功的：
        + 在中间件的内部，再基于 store.dispatch 进行派发
          store.dispatch(promise实例的值「也就是函数返回的action对象」)
        + 而此派发会通知reducer执行，以此来修改状态
    */
    /* 
    async increment(step = 1) {
        await API.delay()
        return {
            type: AT.DEMO_INCREMENT,
            step
        }
    } 
    */

    /* 
    // 同步派发
    increment(step = 1) {
        return {
            type: AT.DEMO_INCREMENT,
            step
        }
    } 
    */
}
export default demoAction