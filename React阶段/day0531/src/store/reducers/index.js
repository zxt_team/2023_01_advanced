import { combineReducers } from 'redux'
import demoReducer from './demoReducer'
import taskReducer from './taskReducer'

const reducer = combineReducers({
    demo: demoReducer,
    task: taskReducer
})
export default reducer