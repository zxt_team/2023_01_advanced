import React from "react"
import styled from "styled-components"
import TitleBoxStyle from "@/assets/TitleBoxStyle"

// 编写样式
const DemoBox = styled.div`
    background: lightpink;
`

export default class Demo2 extends React.Component {
    render() {
        return <DemoBox>
            <TitleBoxStyle size="small">
                我是案例2
            </TitleBoxStyle>
        </DemoBox>
    }
}