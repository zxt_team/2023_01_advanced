import React from "react"
import sty from './Demo1.module.less'
console.log(sty)

export default class Demo1 extends React.Component {
    render() {
        return <div className={sty.demo}>
            <h2 className={sty.title}>
                我是案例1
                <span>10</span>
            </h2>
        </div>
    }
}