import { useState, useEffect, useLayoutEffect } from 'react'
import { Button } from 'antd'

export default function Demo() {
    let [x, setX] = useState(10),
        [y, setY] = useState(20),
        [z, setZ] = useState(30)

    /* 
    // useEffect必须处于函数组件的顶层「不能嵌入到判断/循环等操作中」
    /!* if (x > 12) {
        useEffect(() => {
            console.log('AAA')
        })
    } *!/
    useEffect(() => {
        if (x > 12) {
            console.log('AAA')
        }
    }, [x]) 
    */

    /* // 在useEffect传递的 callback 函数中，不写返回值或者只能返回一个函数，所以导致 callback 不能基于 async 修饰「因为一但修饰，则直接返回 promise 实例」
    const query = async () => {
        await api.query()
    }
    useEffect(() => {
        // (async () => {
        //     await api.query()
        // })()
        query()
    }, []) */

    return <div className="demo" style={{ padding: '50px' }}>
        <p>{x} - {y} - {z}</p>
        <Button type='primary' size='small' onClick={() => setX(x + 1)}>修改X</Button>
        <Button type='primary' size='small' onClick={() => setY(y + 1)}>修改Y</Button>
        <Button type='primary' size='small' onClick={() => setZ(z + 1)}>修改Z</Button>
    </div>
}