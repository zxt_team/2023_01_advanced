import { useState, useEffect, createRef, useRef } from 'react'

let prev
export default function Demo() {
    let [, setRandom] = useState(0)
    let spanBox = useRef()
    if (!prev) {
        prev = spanBox //把第一次渲染创建的REF对象赋值给 prev
    } else {
        // 在组件更新后，函数会重新执行，遇到 createRef ，又创建一个REF对象，此时我们对比两次创建是的是否是相同的对象！！
        console.log(prev === spanBox)
    }

    useEffect(() => {
        console.log(spanBox.current)
    })

    return <div>
        <span ref={spanBox}>哈哈哈</span>
        <button onClick={() => setRandom(+new Date())}>更新</button>
    </div>
}

/* export default function Demo() {
    let spanBox1 = null
    let spanBox2 = createRef()
    let spanBox3 = useRef()

    useEffect(() => {
        console.log(spanBox1) //第一个span
        console.log(spanBox2.current) //第二个span
        console.log(spanBox3.current) //第三个span
    }, [])

    return <div>
        <span ref={x => spanBox1 = x}>01</span>
        <span ref={spanBox2}>02</span>
        <span ref={spanBox3}>03</span>
    </div>
} */