import React from "react"
import withRouter from "@/router/withRouter"
import qs from 'qs'

const Message = function Message({ location, params }) {
    /* // @1 接收问号传参信息
    console.log(
        location.search,
        qs.parse(location.search.substring(1))
    ) */

    /* // @2 接收隐式传参信息
    console.log(location.state) */

    /* // @3 获取路径参数信息
    console.log(params) */

    return <div>
        控制面板 - 消息中心
    </div>
}
export default withRouter(Message)