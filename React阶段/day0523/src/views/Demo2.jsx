import { useState, useEffect, useLayoutEffect } from 'react'
import { Button } from 'antd'

export default function Demo() {
    let [x, setX] = useState(10),
        [y, setY] = useState(20),
        [z, setZ] = useState(30)

    useEffect(() => {
        console.log('@1 第一次渲染完毕/更新完毕', x, y, z)
        return () => {
            console.log('@5 组件更新之前', x, y, z);
        }
    })

    useEffect(() => {
        console.log('@2 第一次渲染完毕', x, y, z)
        setTimeout(() => {
            console.log('哈哈哈', x, y, z) //获取的永远是闭包1中的状态值 10/20/30
        }, 5000)
        return () => {
            console.log('@4 组件销毁之前', x, y, z);
        }
    }, [])

    useEffect(() => {
        console.log('@3 第一次渲染完毕 & x/y状态改变', x, y, z)
    }, [x, y])

    return <div className="demo" style={{ padding: '50px' }}>
        <p>{x} - {y} - {z}</p>
        <Button type='primary' size='small' onClick={() => setX(x + 1)}>修改X</Button>
        <Button type='primary' size='small' onClick={() => setY(y + 1)}>修改Y</Button>
        <Button type='primary' size='small' onClick={() => setZ(z + 1)}>修改Z</Button>
    </div>
}