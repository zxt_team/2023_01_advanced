import React from 'react'
import ReactDOM from 'react-dom/client'
import DemoTwo from './views/DemoTwo'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <DemoTwo title="哈哈哈" x={10} y />
)
/*
console.log(
  React.createElement(
    DemoTwo,
    {
      title: "\u54C8\u54C8\u54C8",
      x: 10,
      y: true
    }
  )
)
*/

//======================
/*
import DemoOne from './views/DemoOne'
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <>
    <DemoOne title="哈哈哈" x="10">
      <span name="slot2">嘿嘿嘿</span>
      <span name="slot1">哇咔咔</span>
    </DemoOne>
  </>
)
setTimeout(() => {
  root.render(
    <>
      <DemoOne title="嘿嘿嘿" x="100"></DemoOne>
    </>
  )
}, 2000)
*/

/*
let vd = React.createElement(
  DemoOne,
  {
    title: "\u54C8\u54C8\u54C8",
    x: 10
  },
  React.createElement("span", null, "\u563F\u563F\u563F")
)
console.log(Object.isFrozen(vd.props))
*/

//======================
/* 
 如何调用组件：先导入、再调用
   + React中的组件调用，需要基于 <PascalCase> 这种模式调用，不支持 <kebab-case> 模式！
   + 单闭合和双闭合调用的唯一区别：双闭合调用可以传递子节点
   + 调用组件的底层处理机制
     @1 当我们调用组件的时候，首先基于 babel-preset-react-app & React.createElement 把其变为 virtualDOM
       + type 普通函数/构造函数
       + props 包含了调用组件时设置的属性「如果用双闭合方式调用，并且有子节点，则props中会新增一个 children 字段，存储子节点的信息（一个值或者一个数组）」 => 解析出来的props对象是只读的「被冻结的」
       + key/ref
       + ...
     @2 基于render对virtualDOM进行渲染
       如果 type 是一个普通函数，说明调用的是函数组件
         + 把函数执行，把解析出来 props 作为实参传递给函数
         + 接收函数执行的返回值（一般是一个新的VirtualDOM），最后再把这个返回值进行渲染（渲染为真实DOM）
       如果 type 是一个构造函数，说明调用的是类组件「而且是继承了React.Compontent/PureCompontent的类组件」
         + 基于 new 创建类组件的实例「其内部要经过一套复杂的处理」，并把解析出来的 props 也传递进去
         + 调用实例的 render 方法「类组件就是在这个方法中构建需要的视图的」，接收返回值「一般是一个新的VirtualDOM」，最后基于 render 把其渲染为真实DOM！
*/