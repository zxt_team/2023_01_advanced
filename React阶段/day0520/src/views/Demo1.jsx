import React from "react"
import { Tag, Button } from 'antd'
import _ from '@/assets/utils'

export default class Demo extends React.PureComponent {
    state = {
        arr: [10, 20, 30]
    }
    handler = () => {
        let { arr } = this.state
        arr.push(arr[arr.length - 1] + 10)
        this.setState({
            arr: [...arr]
        })
    }
    render() {
        let { arr } = this.state
        return <div className="demo">
            {arr.map((item, index) => {
                return <Tag key={index}>
                    {item}
                </Tag>
            })}
            <Button type="primary" size="small" onClick={this.handler}>新增</Button>
        </div>
    }
}


/* // 两个对象的浅比较
const shallowEqual = function shallowEqual(objA, objB) {
    // 首先判断两个值是否相等，如果直接就相等了，则没有必要比较「如果传递的是两个对象，并且相等，则说明链各个对象共用相同的堆内存地址」
    if (Object.is(objA, objB)) return true
    // 如果A/B中有任意一个不是对象，则没有办法进行后续的比较，直接返回不相等即可
    if (!_.isObject(objA) || !_.isObject(objB)) return false
    let objAKeys = Reflect.ownKeys(objA),
        objBKeys = Reflect.ownKeys(objB)
    // A/B对象的成员数量都不一致，则肯定不相等
    if (objAKeys.length !== objBKeys.length) return false
    for (let i = 0; i < objAKeys.length; i++) {
        let key = objAKeys[i]
        // A中有的成员，但是在B中没有，说明A和B是不相等的
        if (!objB.hasOwnProperty(key)) return false
        // A/B都有这个成员，但是成员值不同，则说明A和B也是不相等的
        if (!Object.is(objA[key], objB[key])) return false
    }
    // 如果全部比较完，没有发现不一样的地方，则A和B是相等的
    return true
}
export default class Demo extends React.Component {
    state = {
        arr: [10, 20, 30]
    }
    handler = () => {
        let { arr } = this.state
        arr.push(arr[arr.length - 1] + 10)
        this.setState({
            arr:[...arr]
        })
    }
    shouldComponentUpdate(nextProps, nextState) {
        let { props, state } = this
        // 如果新老属性和状态，经过浅比较之后，发现一模一样，则直接返回false，停止继续更新
        if (shallowEqual(props, nextProps) && shallowEqual(state, nextState)) return false
        // 但凡有不一样的地方，则允许更新
        return true
    }
    render() {
        console.log('render')
        let { arr } = this.state
        return <div className="demo">
            {arr.map((item, index) => {
                return <Tag key={index}>
                    {item}
                </Tag>
            })}
            <Button type="primary" size="small" onClick={this.handler}>新增</Button>
        </div>
    }
} */