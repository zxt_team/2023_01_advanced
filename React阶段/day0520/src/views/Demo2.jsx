import React from "react"
import { Button } from 'antd'

export default class Demo extends React.Component {
    state = {
        x: 10,
        y: 20,
        z: 30
    }

    handler = () => {
        setTimeout(() => {
            this.setState({ x: 100 })
            console.log(this.state.x) //10
            this.setState({ y: 200 })
            console.log(this.state.y) //20
            this.setState({ z: 300 })
            console.log(this.state.z) //30
        }, 1000)
    }

    render() {
        console.log('render')
        let { x, y, z } = this.state
        return <div className="box">
            <p> {x} - {y} - {z} </p>
            <Button type="primary" size="small" onClick={this.handler}>按钮</Button>
        </div>
    }
}