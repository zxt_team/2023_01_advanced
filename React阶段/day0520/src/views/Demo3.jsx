import React from "react"
import { Button } from 'antd'

export default class Demo extends React.Component {
    state = {
        x: 0
    }

    handler = () => {
        /* // 结果：更新一次，最后结果是 1
        for (let i = 0; i < 10; i++) {
            this.setState({
                x: this.state.x + 1
            })
        } */

        /* // 结果：更新一次，最后结果是 10
        let x = this.state.x
        for (let i = 0; i < 10; i++) {
            x = x + 1
            this.setState({
                x
            })
        } */

        /* // 结果：更新一次，最后结果是 10
        for (let i = 0; i < 10; i++) {
            this.state.x++  //每一轮循环，直接手动的修改状态值「不会更新视图」
            this.setState({}) //通知视图更新{或者换成 this.forceUpdate()}
        } */

        /* 
        setState的第一个参数可以是一个对象，也可以是一个函数 
          如果是个对象：把x状态值改为1「异步」
          this.setState({
            x:1
          })

          如果是个函数：则把函数加入到 updater 队列中，当后期通知队列中的任务执行时，会依次把存放的函数执行，把当前处理好的状态传递给 prev，接收函数的返回值，作为要修改的状态信息！
          this.setState((prev)=>{
            //prev：存储的是上一个状态信息
            return { //返回的对象是我们需要修改的状态值
                x:1
            }
          })
        */
        for (let i = 0; i < 10; i++) {
            this.setState((prev) => {
                return {
                    x: prev.x + 1
                }
            })
        }
    }

    render() {
        console.log('render')
        let { x } = this.state
        return <div className="box">
            <p> {x} </p>
            <Button type="primary" size="small" onClick={this.handler}>按钮</Button>
        </div>
    }
}