import React from "react"
import { flushSync } from 'react-dom'
import { Button } from 'antd'

export default class Demo extends React.Component {
    state = {
        x: 10,
        y: 20,
        z: 0
    }

    handler = () => {
        /* // 更新2次，z变为300
        this.setState({ x: 100 })
        console.log(this.state.x) //10
        this.setState({ y: 200 })
        console.log(this.state.y) //20
        setTimeout(() => {
            this.setState({
                z: this.state.x + this.state.y
            })
            console.log(this.state.z) //0
        }) */

        /* // 使用函数的方法「可以保证只更新一次」
        this.setState({ x: 100 })
        console.log(this.state.x) //10
        this.setState({ y: 200 })
        console.log(this.state.y) //20
        this.setState(prev => {
            // prev : {x:100,y:200,z:0}
            return {
                z: prev.x + prev.y
            }
        })
        console.log(this.state.z) //0 */

        /*
         flushSync(()=>{ ... }) 
         flushSync是用来刷新 updater 队列的「所谓刷新队列，就是让队列中的任务立即更新一次」
         代码执行中，如果遇到 flushSync ，则把之前加入 updater 队列中的，以及 flushSync回调函数中，加入 updater 队列中的，统一更新一次！更新完毕后，才会继续向下执行！
         真实项目中，我们习惯性的把：所有需要立即刷新一次的任务，都放在 flushSync 的回调函数中
         */
        flushSync(() => {
            this.setState({ x: 100 })
            console.log(this.state.x) //10
            this.setState({ y: 200 })
            console.log(this.state.y) //20
        })
        console.log(this.state)
        this.setState({
            z: this.state.x + this.state.y
        })
        console.log(this.state.z) //0
    }

    render() {
        console.log('render')
        let { x, y, z } = this.state
        return <div className="box">
            <p> {x} - {y} - {z} </p>
            <Button type="primary" size="small" onClick={this.handler}>按钮</Button>
        </div>
    }
}