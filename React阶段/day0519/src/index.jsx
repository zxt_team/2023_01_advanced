import React from 'react'
import ReactDOM from 'react-dom/client'
import TodoList from './views/TodoList'
/* ANTD */
import { ConfigProvider } from 'antd'
import zhCN from 'antd/locale/zh_CN'
import './index.less'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <ConfigProvider locale={zhCN}>
    <TodoList />
  </ConfigProvider>
)