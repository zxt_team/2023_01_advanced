import React from 'react'
import { Button, Input, Popconfirm, message } from 'antd'
import _ from '@/assets/utils'
import './TodoList.less'

export default class TodoList extends React.Component {
    state = {
        taskText: '',
        taskList: []
    }

    // 第一次渲染完毕，我们需要观察本地是否存储了任务列表，如果存储了，则赋值给taskList状态
    componentDidMount() {
        let list = _.storage.get('CACHE_TASKLIST')
        if (list) {
            this.setState({
                taskList: list
            })
        }
    }

    // 监听任务列表的变化，把最新的信息存储到本地
    watchTaskList = () => {
        _.storage.set('CACHE_TASKLIST', this.state.taskList)
    }

    // 新增任务
    submit = () => {
        let { taskText, taskList } = this.state
        if (taskText.length === 0) {
            message.warning(`任务描述不能为空哦`)
            return
        }
        taskList.push({
            id: +new Date(), //任务编号「唯一的」
            text: taskText, //任务描述信息
            isUpdate: false, //是否为修改状态
            updateText: '' //在修改状态下，文本框中输入的最新信息
        })
        this.setState({
            taskList,
            taskText: ''
        }, this.watchTaskList)
    }

    // 删除任务
    removeTask = (id) => {
        let { taskList } = this.state
        taskList = taskList.filter(item => +item.id !== +id)
        this.setState({ taskList }, this.watchTaskList)
    }

    // 触发修改态
    triggerUpdate = (id, type) => {
        let { taskList } = this.state
        taskList = taskList.map(item => {
            if (+item.id === +id) {
                if (type === 'cancel') {
                    item.isUpdate = false
                    item.updateText = ''
                } else {
                    item.isUpdate = true
                    item.updateText = item.text
                }
            }
            return item
        })
        this.setState({ taskList }, this.watchTaskList)
    }

    // 保存
    saveInfo = (id) => {
        let { taskList } = this.state
        let item = taskList.find(item => +item.id === +id)
        if (!item) return
        if (item.updateText.length === 0) {
            message.warning(`任务描述不能为空哦`)
            return
        }
        item.text = item.updateText
        item.isUpdate = false
        this.setState({ taskList }, this.watchTaskList)
    }

    render() {
        let { taskText, taskList } = this.state
        return <div className="todo-box">
            <div className="header">
                <Input placeholder="请输入任务描述"
                    value={taskText}
                    onChange={ev => {
                        this.setState({
                            taskText: ev.target.value.trim()
                        })
                    }} />
                <Button type="primary" onClick={this.submit}>新增任务</Button>
            </div>
            <ul className="main">
                {taskList.map(item => {
                    let { id, text, isUpdate, updateText } = item
                    return <li className="item" key={id}>
                        <p className="text">
                            {isUpdate ?
                                <Input size="small"
                                    value={updateText}
                                    onChange={ev => {
                                        let val = ev.target.value.trim()
                                        item.updateText = val
                                        this.setState({ taskList }, this.watchTaskList)
                                    }} /> :
                                <span>{text}</span>}
                        </p>
                        <p className="handle">
                            <Popconfirm title="您确定要删除此任务吗？"
                                onConfirm={this.removeTask.bind(null, id)}>
                                <Button type="primary" size="small" danger>删除</Button>
                            </Popconfirm>
                            {isUpdate ?
                                <>
                                    <Button type="primary" size="small"
                                        onClick={this.saveInfo.bind(null, id)}>
                                        保存
                                    </Button>
                                    <Button size="small"
                                        onClick={this.triggerUpdate.bind(null, id, 'cancel')}>
                                        取消
                                    </Button>
                                </> :
                                <Button type="primary" size="small"
                                    onClick={this.triggerUpdate.bind(null, id, 'update')}>
                                    修改
                                </Button>
                            }
                        </p>
                    </li>
                })}
            </ul>
        </div>
    }
}