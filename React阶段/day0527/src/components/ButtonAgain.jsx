/* 对Antd中Button组件的二次封装：自动加Loading防抖处理 */
import { Button } from 'antd'
import { useState } from 'react'

const ButtonAgain = function ButtonAgain(props) {
    // 把传递的属性拷贝一份，这样就可以直接操作props了
    props = { ...props }
    // onClick:点击时候要干的事{传递的handle函数}
    // children:插槽信息
    // loading:我们可以把传递的loading作为我们自己管理的loading的初始值
    let { onClick, children, loading } = props
    // 把特殊的几个属性值移除掉
    if (props.onClick) delete props.onClick
    if (props.loading) delete props.loading
    if (props.children) delete props.children

    // 自己要做的特殊处理
    let [submitLoading, setSubmitLoading] = useState(!!loading)
    const submit = async (ev) => {
        setSubmitLoading(true)
        try {
            if (typeof onClick === "function") {
                await onClick(ev)
            }
        } catch (_) { }
        setSubmitLoading(false)
    }

    // 剩下的属性，原封不动的给Antd中的Button即可
    return <Button {...props} loading={submitLoading} onClick={submit}>
        {children}
    </Button>
}
export default ButtonAgain