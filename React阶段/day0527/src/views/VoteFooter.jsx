import { Component } from 'react'
import { Button } from 'antd'
import ThemeContext from '@/ThemeContext'

export default class VoteFooter extends Component {
    // 获取上下文中的信息
    static contextType = ThemeContext

    render() {
        let { change } = this.context // this.context存储获取的上下文信息
        return <div className="footer-box">
            <Button type="primary" onClick={change.bind(null, 'sup')}>支持</Button>
            <Button type="primary" danger onClick={change.bind(null, 'opp')}>反对</Button>
        </div>
    }
}