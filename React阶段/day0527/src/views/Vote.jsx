import { useState } from 'react'
import VoteStyle from "./VoteStyle"
import VoteMain from "./VoteMain"
import VoteFooter from "./VoteFooter"
import ThemeContext from "@/ThemeContext"

export default function Vote() {
    let [supNum, setSupNum] = useState(10),
        [oppNum, setOppNum] = useState(5)
    const change = (type) => {
        if (type === 'sup') {
            setSupNum(supNum + 1)
            return
        }
        setOppNum(oppNum + 1)
    }

    return <ThemeContext.Provider
        value={{
            supNum,
            oppNum,
            change
        }}>
        <VoteStyle>
            <h2 className="title">
                React其实也不难!
                <span>{supNum + oppNum}</span>
            </h2>
            <VoteMain />
            <VoteFooter />
        </VoteStyle>
    </ThemeContext.Provider>
}