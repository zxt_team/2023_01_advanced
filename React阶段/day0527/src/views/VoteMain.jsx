import ThemeContext from "@/ThemeContext"
import { useContext } from 'react'

export default function VoteMain() {
    let context = useContext(ThemeContext) //获取所有上下文信息
    let { supNum, oppNum } = context
    return <div className="main-box">
        <p>支持人数：{supNum} 人</p>
        <p>反对人数：{oppNum} 人</p>
    </div>
}

/* export default function VoteMain() {
    return <ThemeContext.Consumer>
        {value => {
            // value：存储的是所有上下文中的信息
            let { supNum, oppNum } = value
            return <div className="main-box">
                <p>支持人数：{supNum} 人</p>
                <p>反对人数：{oppNum} 人</p>
            </div>
        }}
    </ThemeContext.Consumer>
} */