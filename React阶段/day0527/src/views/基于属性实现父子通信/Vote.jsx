import { useState } from 'react'
import VoteStyle from "./VoteStyle"
import VoteMain from "./VoteMain"
import VoteFooter from "./VoteFooter"

export default function Vote() {
    // 父组件具备状态和修改状态的方法
    let [supNum, setSupNum] = useState(10),
        [oppNum, setOppNum] = useState(5)
    const change = (type) => {
        if (type === 'sup') {
            setSupNum(supNum + 1)
            return
        }
        setOppNum(oppNum + 1)
    }

    return <VoteStyle>
        <h2 className="title">
            React其实也不难!
            <span>{supNum + oppNum}</span>
        </h2>
        {/* 基于属性把支持数/反对数传递给子组件 */}
        <VoteMain supNum={supNum} oppNum={oppNum} />
        {/* 基于属性把修改状态的方法传递给子组件 */}
        <VoteFooter change={change} />
    </VoteStyle>
}