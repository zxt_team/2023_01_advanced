import React from "react"
import { Form, Input, Checkbox, Button } from "antd"
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import styled from "styled-components"

/* 组件的样式 */
const LoginStyle = styled.div`
    .remember{
        display: flex;
        justify-content: space-between;
        align-items: center;

        a{
            font-size: 14px;
            color: #1890ff;
        }
    }

    .submit{
        width: 100%;
    }
`

const Login = function Login() {
    return <LoginStyle>
        <Form
            autoComplete="off"
            initialValues={{
                account: '',
                password: ''
            }}>
            <Form.Item name="account">
                <Input size="large" placeholder="请输入账号" prefix={<UserOutlined />} />
            </Form.Item>
            <Form.Item name="password">
                <Input.Password size="large" placeholder="请输入密码" prefix={<LockOutlined />} />
            </Form.Item>
            <Form.Item>
                <div className="remember">
                    <Checkbox checked>记住账号密码</Checkbox>
                    <a href="">忘记密码？</a>
                </div>
            </Form.Item>
            <Form.Item>
                <Button className="submit" type="primary" size="large">立即登录</Button>
            </Form.Item>
        </Form>
    </LoginStyle>
}
export default Login