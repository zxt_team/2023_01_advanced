import React from "react"
import { HashRouter, BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import BasicLayout from "./layout/BasicLayout"
import UserLayout from "./layout/UserLayout"
import Error from "./layout/Error"

/*
  HashRouter/BrowserRouter：控制使用何种路由切换方式，是 Hash路由 还是 History路由？
  Switch：控制如果某一个Route匹配成功，则不再继续向下匹配了
  Redirect：重定向，语法和Route类似，只不过其不需要component，而是基于 to 来指定重定向后的地址，并且基于from来代替path；
  Route：构建路由的匹配机制
    + path 匹配地址
      当路由切换完毕（或页面刷新），会拿最新的路由地址，和Route中的path进行匹配；找到匹配项后，把对应的component组件，在此位置进行渲染！
        路由地址        path值     是否匹配   精准匹配
          /            /          是        是
          /            /user      否        否
          /user        /          是        否
          /user        /user      是        是
          /user2       /user      否        否
          /user/login  /user      是        否
        默认是按照整体进行匹配的「一个完整的“/xxx”算是一个整体」，如果路由地址中有一到多个整体，其中有一部分整体和path值一致，则说明匹配，反之是不匹配！“/”也算一个整体，所以默认情况下“/xxx”都会匹配“/”!
    + exact 开启精准匹配，要求路由地址必须和path一模一样才算匹配成功
    + component 需要渲染的组件

 */

const App = function App() {
    return <HashRouter>
        <Switch>
            <Route path="/user" component={UserLayout} />
            <Route path="/404" component={Error} />
            <Route path="/" component={BasicLayout} />
            <Redirect to="/404" />
        </Switch>
    </HashRouter>
}
export default App