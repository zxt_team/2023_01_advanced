基于前端路由实现SPA单页面应用，其核心在于：构建路由表(或构建路由匹配机制)
  @1 每一次路由切换（包含刚开始渲染页面），都会拿当前的地址（或者哈希值）去路由表中进行查找匹配，找到相匹配的组件
  @2 把找到的组件放在指定的容器中渲染

react-router-dom V5
  @1 和 vue-router 不同，没有单独的路由表，需要把路由匹配机制，写在指定组件的特定位置
    特定位置：哪一块需要根据不同地址渲染不同组件，就把匹配机制写在这