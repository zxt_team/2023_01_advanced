import React from "react"
import styled from "styled-components"
import RouterView from "@/router"
import { Link, NavLink } from 'react-router-dom'

/* 组件样式 */
const HomeStyle = styled.div`
    padding: 10px;

    .nav-box{
        display: flex;
        border-bottom: 1px solid #EEE;

        a{
            padding: 0 20px;
            line-height: 40px;
            font-size: 15px;
            color: #000;

            &.active{
                color: #1677ff;
                font-size: 16px;
            }
        }
    }
`

/*
  Link/NavLink：都是实现路由切换的方式
    <Link to="要跳转的路由地址">渲染的内容</Link>
      + to 属性值是字符串/对象
        to="/home/watch"
        to={{
            pathname:'/home/watch'
        }}
      + replace 如果设置这个属性，则本次路由跳转是替换现有的历史记录，而不是新增记录

    渲染完成的结果依然是A标签
      + 但是它会根据当前路由的模式，自动设置A标签的href属性值
        哈希路由  href='#/home/watch'
        History路由  href='/home/watch'「在内部点击A标签的时候，阻止了A的默认行为，转而基于HistoryAPI实现路由的跳转」

    <NavLink> 其语法和 <Link> 基本一致，主要区别是：
      + 会拿当前路由的地址 和 to要跳转的地址 进行匹配「默认是非精准匹配，不过我们可以设置 exact 让其变为精准匹配」
      + 和哪一项匹配了，就给当前项，设置一个叫做 active 的样式类
        可以基于 activeClassName 修改匹配的样式类名
        也可以直接基于 className/style/activeStyle 设置选中的样式
      + 这样我们就可以给当前选中的项，设置选中样式了！！
 */

const Home = function Home() {
    return <HomeStyle>
        <nav className="nav-box">
            <NavLink to="/home/watch">数据监控</NavLink>
            <NavLink to="/home/worker">工作台</NavLink>
            <NavLink to="/home/message">消息中心</NavLink>
        </nav>
        {/* 首页的三级路由 */}
        <RouterView name="home" />
    </HomeStyle>
}
export default Home