/* 
 路由表中的每一项都是对象「参照着vue-router的规则来」
   path:'' 路由地址「也是redirect模式下的from」
   name:'' 路由名字
   component  路由匹配后需要渲染的组件
   render   某些情况下，传递的不是component而是render
   exact  是否开启精准匹配
   redirect  设置redirect后，则开启重定向模式，redirect存储的是重定向后的地址
   meta:{}  路由元信息「我们自己给每一个路由设置的自定义信息」
   children 子路由信息，如果有子路由，则值是一个数组
 */
import UserLayout from "@/layout/UserLayout"
import Login from "@/views/Login"
import Register from "@/views/Register"

import Error from "@/layout/Error"

import BasicLayout from "@/layout/BasicLayout"
import Home from "@/views/Home"
import Category from "@/views/Category"
import Personal from "@/views/Personal"

import Watch from "@/views/home/Watch"
import Worker from "@/views/home/Worker"
import Message from "@/views/home/Message"

// User下的二级路由
const userChildren = [
    {
        path: '/user',
        exact: true,
        redirect: '/user/login'
    },
    {
        path: '/user/login',
        name: 'login',
        meta: { title: '用户登录' },
        component: Login
    },
    {
        path: '/user/register',
        name: 'register',
        meta: { title: '用户注册' },
        component: Register
    }
]

// Home（控制面板）下的三级路由
const homeChildren = [
    {
        path: '/home',
        exact: true,
        redirect: '/home/watch'
    },
    {
        path: '/home/watch',
        name: 'watch',
        meta: { title: '数据监控' },
        component: Watch
    },
    {
        path: '/home/worker',
        name: 'worker',
        meta: { title: '工作台' },
        component: Worker
    },
    {
        path: '/home/message',
        name: 'message',
        meta: { title: '消息中心' },
        component: Message
    }
]

// Basic（主页）下的二级路由
const basicChildren = [
    {
        path: '/',
        exact: true,
        redirect: '/home'
    },
    {
        path: '/home',
        name: 'home',
        meta: { title: '控制面板' },
        component: Home,
        children: homeChildren
    },
    {
        path: '/category',
        name: 'category',
        meta: { title: '分类管理' },
        component: Category
    },
    {
        path: '/personal',
        name: 'personal',
        meta: { title: '个人中心' },
        component: Personal
    }
]

// 一级路由
const routes = [
    {
        path: '/user',
        name: 'user',
        meta: {},
        component: UserLayout,
        children: userChildren
    },
    {
        path: '/404',
        name: 'error',
        meta: { title: '404错误页' },
        component: Error
    },
    {
        path: '/',
        name: 'basic',
        meta: {},
        component: BasicLayout,
        children: basicChildren
    }
]
export default routes