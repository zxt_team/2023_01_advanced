import React from 'react'
import ReactDOM from 'react-dom/client'
import { createElement, render } from './source'

/* const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <div className="box" id="box" key={10} style={{ color: 'lightblue' }}>
    <h2 className="title">我是标题</h2>
    <p>我是段落</p>
  </div>
) */

let virtualDOM = createElement(
  "div",
  {
    className: "box",
    id: "box",
    key: 10,
    style: {
      color: 'lightblue'
    }
  },
  createElement(
    "h2",
    { className: "title" },
    "\u6211\u662F\u6807\u9898"
  ),
  createElement(
    "p",
    null,
    "\u6211\u662F\u6BB5\u843D"
  )
)
render(virtualDOM, document.getElementById('root'))

/* let title = '珠峰培训React课程'
let arr = ['选项1', '选项2', '选项3']
let isExist = false
let objSty = {
  color: 'red'
}
let str = `我的名字是：哈哈哈! <a href=''>呵呵呵</>`

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <div>
    <div dangerouslySetInnerHTML={{
      __html: str
    }}></div>

    <h2 className="biaoti" style={objSty}>{title}</h2>
    {isExist ? <button>按钮</button> : null}
    <div>
      {
        // arr:[<li key={0}>选项1</li>,...]
        arr.map((item, index) => {
          return (
            <React.Fragment key={index}>
              <i>{index}</i>
              <span>
                {item}
              </span>
            </React.Fragment>
          )
        })
      }
    </div>
  </div>
) */