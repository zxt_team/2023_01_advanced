import _ from '@/assets/utils'

/* createElement：创建虚拟DOM对象 */
export const createElement = function createElement(type, props, ...children) {
    let virtualDOM = {
        $$typeof: Symbol('react.element'),
        type,
        key: null,
        ref: null,
        props: {}
    }
    // 传递的 props 有值：迭代对象中的每一项，把key/ref单独处理，其余的都赋值给 virtualDOM.props
    if (_.isPlainObject(props)) {
        _.each(props, (value, key) => {
            if (key === 'ref' || key === 'key') {
                virtualDOM[key] = value
                Object.defineProperty(virtualDOM.props, key, {
                    configurable: true,
                    enumerable: false,
                    get() {
                        console.error(`Waring: ${type}: '${key}' is not a prop`)
                    }
                })
                return
            }
            virtualDOM.props[key] = value
        })
    }
    // 处理传递的 children 子节点
    let len = children.length
    if (len > 0) {
        virtualDOM.props.children = len === 1 ? children[0] : children
    }
    return virtualDOM
}

/* render：把虚拟DOM变为真实DOM */
export const render = function render(virtualDOM, container) {
    let { type, props } = virtualDOM
    // 创建元素标签（真实DOM）
    if (typeof type === 'string') {
        let elem = document.createElement(type)
        Object.keys(props).forEach(key => {
            let value = props[key]
            if (key === 'className') {
                elem.setAttribute('class', value)
                return
            }
            if (key === 'style') {
                // value：样式对象，我们需要分别赋值元素的 style 行内样式
                _.each(value, (styV, styK) => {
                    elem.style[styK] = styV
                })
                return
            }
            if (key === 'children') {
                // 处理元素的子节点，value 是children属性的值
                let children = !Array.isArray(value) ? [value] : value
                children.forEach(child => {
                    // child：每个子节点「文本|元素」
                    if (typeof child === 'string') {
                        // 文本子节点
                        let textNode = document.createTextNode(child)
                        elem.appendChild(textNode)
                        return
                    }
                    // 元素子节点：递归处理
                    render(child, elem)
                })
                return
            }
            elem.setAttribute(key, value)
        })
        container.appendChild(elem)
    }
    // 这是一个组件
    /* if (typeof type === 'function') {
        // 如果是类组件
        if (type.prototype.isReactComponent) {
            let inst = new type(props)
            //....
            let newVirtualDOM = inst.render()
            render(newVirtualDOM, container)
            return
        }
        // 如果是函数组件
        let newVirtualDOM = type(props)
        render(newVirtualDOM, container)
    } */
}