const sleep = function sleep(interval = 1000) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(interval)
        }, interval)
    })
}
/* 需求：串行发送三个请求{等待时间分别为:1000/2000/3000} */

/* // 方案一：基于 promise 语法处理
sleep(1000)
    .then(value => {
        console.log(`第一个请求成功，请求结果是：${value}`)
        return sleep(2000)
    })
    .then(value => {
        console.log(`第二个请求成功，请求结果是：${value}`)
        return sleep(3000)
    })
    .then(value => {
        console.log(`第三个请求成功，请求结果是：${value}`)
    }) */

/* // 方案二：基于 async/await 语法糖（promise+generator的语法糖）处理
const handle = async () => {
    let value = await sleep(1000)
    console.log(`第一个请求成功，请求结果是：${value}`)

    value = await sleep(2000)
    console.log(`第二个请求成功，请求结果是：${value}`)

    value = await sleep(3000)
    console.log(`第三个请求成功，请求结果是：${value}`)
}
handle() */

// 方案三：基于 generator 处理
const AsyncFunction = function AsyncFunction(generator, ...params) {
    return new Promise(resolve => {
        // generator：需要管控处理的生成器函数  params：以后执行 generator ，为其传递的实参
        let itor = generator(...params)
        // 递归一次次的执行 next ，控制生成器函数中的代码一步步执行
        const recursion = (x) => {
            let { value, done } = itor.next(x)
            if (done) {
                // 生成器函数执行完毕，结束递归操作，value就是生成器函数最后的返回值
                resolve(value)
                return
            }
            if (!(value instanceof Promise)) value = Promise.resolve(value) //确保每一次yield后面的值都是promise实例
            value.then(x => {
                // 等待上一步处理成功(x是成功的结果)，则开启下一步的执行
                recursion(x)
            })
        }
        recursion()
    })
}
AsyncFunction(
    function* generator() {
        let value = yield sleep(1000)
        console.log(`第一个请求成功，请求结果是：${value}`)
        value = yield sleep(2000)
        console.log(`第二个请求成功，请求结果是：${value}`)
        value = yield sleep(3000)
        console.log(`第三个请求成功，请求结果是：${value}`)
        return '哈哈哈'
    }
).then(value => {
    console.log(value)
})


/* 
function* generator() {
    let value = yield sleep(1000)
    console.log(`第一个请求成功，请求结果是：${value}`)

    value = yield sleep(2000)
    console.log(`第二个请求成功，请求结果是：${value}`)

    value = yield sleep(3000)
    console.log(`第三个请求成功，请求结果是：${value}`)
}
let itor = generator()
let { value, done } = itor.next()  //value：第一个请求的promise实例  done:false
value.then(x => {
    // x:第一个请求成功的结果
    let { value, done } = itor.next(x) //value：第二个请求的promise实例  done:false
    value.then(x => {
        // x:第二个请求成功的结果
        let { value, done } = itor.next(x) //value：第三个请求的promise实例  done:false
        value.then(x => {
            // x:第三个请求成功的结果
            let { value, done } = itor.next(x) //value：undefined  done:true
        })
    })
}) 
*/