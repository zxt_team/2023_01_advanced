import { lazy } from 'react'
import BasicLayout from "@/layout/BasicLayout"

// User下的二级路由
const userChildren = [
    {
        path: '/user',
        exact: true,
        redirect: '/user/login'
    },
    {
        path: '/user/login',
        name: 'login',
        meta: { title: '用户登录' },
        component: lazy(() => import(/*webpackChunkName:'user'*/ '@/views/Login'))
    },
    {
        path: '/user/register',
        name: 'register',
        meta: { title: '用户注册' },
        component: lazy(() => import(/*webpackChunkName:'user'*/ '@/views/Register'))
    }
]

// Home（控制面板）下的三级路由
const homeChildren = [
    {
        path: '/home',
        exact: true,
        redirect: '/home/watch'
    },
    {
        path: '/home/watch',
        name: 'watch',
        meta: { title: '数据监控' },
        component: lazy(() => import(/*webpackChunkName:'home'*/ '@/views/home/Watch'))
    },
    {
        path: '/home/worker',
        name: 'worker',
        meta: { title: '工作台' },
        component: lazy(() => import(/*webpackChunkName:'home'*/ '@/views/home/Worker'))
    },
    {
        path: '/home/message/:lx?/:name?',
        name: 'message',
        meta: { title: '消息中心' },
        component: lazy(() => import(/*webpackChunkName:'home'*/ '@/views/home/Message'))
    }
]

// Basic（主页）下的二级路由
const basicChildren = [
    {
        path: '/',
        exact: true,
        redirect: '/home'
    },
    {
        path: '/home',
        name: 'home',
        meta: { title: '控制面板' },
        component: lazy(() => import(/*webpackChunkName:'home'*/ '@/views/Home')),
        children: homeChildren
    },
    {
        path: '/category',
        name: 'category',
        meta: { title: '分类管理' },
        component: lazy(() => import(/*webpackChunkName:'category'*/ '@/views/Category'))
    },
    {
        path: '/personal',
        name: 'personal',
        meta: { title: '个人中心' },
        component: lazy(() => import(/*webpackChunkName:'personal'*/ '@/views/Personal'))
    }
]

// 一级路由
const routes = [
    {
        path: '/user',
        name: 'user',
        meta: {},
        component: lazy(() => import(/*webpackChunkName:'user'*/ '@/layout/UserLayout')),
        children: userChildren
    },
    {
        path: '/404',
        name: 'error',
        meta: { title: '404错误页' },
        component: lazy(() => import('@/layout/Error'))
    },
    {
        path: '/',
        name: 'basic',
        meta: {},
        component: BasicLayout,
        children: basicChildren
    }
]
export default routes