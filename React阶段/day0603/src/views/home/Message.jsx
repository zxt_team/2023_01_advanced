import React from "react"
import qs from 'qs'
import { useParams } from 'react-router-dom'

const Message = function Message({ location, match }) {
    // @1 接收问号传参的信息
    /* let { search } = location
    search = qs.parse(search.replace('?', ''))
    console.log(search) */

    // @2 接收隐式传参的信息
    /* let { state } = location
    console.log(state) */

    // @3 接收路径参数的信息
    // console.log(match.params, useParams())


    /* 
    // 扩展知识：对问号传参的信息(字符串 xxx=xxx&&xxx=xxx URLENCODED格式字符串)进行处理
    // 1. 基于内置的方法处理
    let searchObj = new URLSearchParams(search)
    console.log(searchObj.get('name')) //'zhufeng'

    // 2. 可以基于 qs 库处理「安装axios自带的库」
    let searchObj = qs.parse(search.replace('?', ''))  //把字符串变为对象
    console.log(searchObj, search)
    let obj = { lx: 1, name: 'AAA' }
    console.log(qs.stringify(obj)) //lx=1&name=AAA  把对象变为字符串 
    */

    return <div>
        控制面板 - 消息中心
    </div>
}
export default Message