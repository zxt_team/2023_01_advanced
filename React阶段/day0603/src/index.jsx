import React from 'react'
import ReactDOM from 'react-dom/client'
/* ANTD */
import { ConfigProvider } from 'antd'
import zhCN from 'antd/locale/zh_CN'
/* 组件&样式 */
import './index.less'
import App from './App'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <ConfigProvider locale={zhCN}>
    <App />
  </ConfigProvider>
)