/*
 async/await：简化promise操作的语法糖「promise+generator的语法糖」
   async的作用「修饰函数」：
     + 让函数的返回值变为一个promise实例
       方法执行中遇到await，会等待await的处理结果
       + 方法执行报错，则返回失败的实例，值是报错原因
       + 看方法执行的返回值，如果不是一个新的实例，则返回的实例是成功，值是返回值；如果返回的是一个新的实例，则返回的实例和这个新实例保持同步！！
     + 想要在函数中使用await，则当前函数必须经过async的修饰

   await会等待一个promise的处理结果
     let result = await promise实例
      + await后面必须跟一个promise实例
         await fn() 先执行fn函数，执行的返回值作为await等待的操作
         await 10  如果不是promise实例，则默认转换为状态为成功，值是本身的promise实例 -> await Promsie.resolve(10)
      + 如果后面的实例是成功状态
        result 存储的就是成功的结果
        当前上下文中，下面代码可以继续执行
      + 如果后面的实例是失败状态，则下面的代码就不会执行了
 */

// await 也是一个异步的微任务「它仅仅是在函数中，模拟出了同步的效果」
console.log(1)
~(async () => {
    console.log(2)
    await Promise.resolve()
    console.log(3)
})()
console.log(4)


//==================
/* const fn = async () => {
    // await 操作的异常捕获「即便await后面的时候是失败的，也不会抛出异常错误了，try/catch下面的代码还可以继续执行」
    try {
        await Promise.reject(0)
        // ... 后面实例为成功干的事情
        console.log('OK')
    } catch (_) {
        // ... 后面实例为失败干的事情
        console.log('NO')
    }
    return 10
}
fn().then(value => {
    console.log('成功：', value)  //成功 10
}).catch(reason => {
    console.log('失败：', reason)
}) */

/* const fn = async () => {
    await Promise.reject(0)
    return 10
}
fn().then(value => {
    console.log('成功：', value)
}).catch(reason => {
    console.log('失败：', reason) //await 后面的实例是失败的，而且我们没有对失败进行处理，所以抛出异常，这样就导致fn执行返回的实例也是失败态，值就是抛出的异常（也就是await后面实例失败的原因）
}) */

/* const fn = async () => {
    await _.delay()
    return 10
}
fn().then(value => {
    console.log('成功：', value) //等待await处理完毕，fn返回的实例变为成功，此处才会执行
}) */

/* (async () => {
    let result = await Promise.reject(0)
    console.log(result) //下面代码不会执行，并且抛出一个不影响后续操作的错误 Uncaught (in promise)
})() */

/* (async () => {
    let result = await Promise.resolve(100)
    console.log(result) //100
})() */

/* async function fn() {
    return 10
}
console.log(fn())  */